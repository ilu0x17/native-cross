#!/bin/bash

set -x

DEFAULT_BUILDCMD='dpkg-buildpackage -us -uc'

if [ $# -lt 1 ]; then
    WORK="$(realpath "$PWD/..")"
    PACKAGE="$(basename "$PWD")"
else
    WORK="$(realpath "${@: -1}/..")"
    PACKAGE="$(basename "$(realpath "${@: -1}")")"
fi

if [ $# -lt 2 ]; then
    BUILD_CMD="$DEFAULT_BUILDCMD"
else
    BUILD_CMD="$1"
fi

CONTAINER="native-cross"
CHROOT="/chroot/amd64"

docker volume create "$CONTAINER-ccache"

exec docker run \
    --rm \
    --cpu-shares 512 \
    -v "$WORK:/work" \
    -v "$WORK:$CHROOT/work" \
    --mount "type=volume,src=$CONTAINER-ccache,dst=$CHROOT/root/.cache/ccache" \
    -e PACKAGE="$PACKAGE" \
    -e BUILD_CMD="$BUILD_CMD" \
    -e CHROOT="$CHROOT" \
    -it \
    "${CONTAINER}"
