#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>

const char *CHROOT = "/chroot/amd64/";

const char *str_last_match(const char *str, const char delim)
{
    const char *last = str;
    const char *p = last;
    while (*p != '\0')
    {
        if (*p == delim)
        {
            do
            {
                p++;
            } while (*p == delim);
            if (*p != '\0')
            {
                last = p;
            }
        }
        p++;
    }
    return last;
}

int main(int argc, char *argv[])
{
    const char *exe = str_last_match(argv[0], '/');
    const char *cwd = get_current_dir_name();

    const char* chroot_path = getenv("CHROOT");
    if (chroot_path) {
        CHROOT = chroot_path;
    }

    int ret = chroot(CHROOT);
    if (ret < 0)
    {
        fprintf(stderr, "ERROR: Can switch root to: `%s': %s\n", CHROOT, strerror(errno));
        exit(EXIT_FAILURE);
    }

    ret = chdir(cwd);
    if (ret < 0)
    {
        fprintf(stderr, "ERROR(chroot): Can't switch to directory: %s: %s\n", cwd, strerror(errno));
        exit(EXIT_FAILURE);
    }

    ret = execvp(exe, argv);
    if (ret < 0)
    {
        fprintf(stderr, "ERROR(chroot): Can't exec `%s'': %s\n", exe, strerror(errno));
        exit(EXIT_FAILURE);
    }

    return 0;
}