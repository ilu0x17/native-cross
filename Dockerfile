FROM arm64v8/debian:bookworm-slim

ARG DEBIAN_FRONTEND=noninteractive
ARG CHROOT=/chroot/amd64

# install build basics
RUN apt update \
&& apt dist-upgrade -y \
&& apt -y install \
	debootstrap \
	apt-utils \
	build-essential \
	fakeroot \
	devscripts \
	equivs \
	git-buildpackage \
	eatmydata \
	bc \
	debhelper \
	rsync \
	kmod \
	cpio \
	bison \
	flex \
	libssl-dev \
&& apt-get clean \
&& rm -rf /var/lib/apt/lists/* \
&& install -d /work

RUN install -d $CHROOT \
&& debootstrap --arch amd64 --variant=minbase bookworm $CHROOT/ http://deb.debian.org/debian/

COPY ./00no-recommends $CHROOT/etc/apt/apt.conf.d/
RUN chroot $CHROOT apt -y update \
&& chroot $CHROOT apt -y install \
	eatmydata \
	gcc-aarch64-linux-gnu \
	ccache \
	xz-utils \
&& chroot $CHROOT apt-get clean \
&& rm -rf $CHROOT/var/lib/apt/lists/*

RUN mkdir -p $CHROOT/work \
&& mkdir -p $CHROOT/root/.cache/ccache

COPY ./chroot_exec /usr/local/bin
RUN ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-addr2line \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-ar \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-as \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-c++filt \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-cpp \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-cpp-12 \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-dwp \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-elfedit \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-g++ \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-g++-12 \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-gcc \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-gcc-12 \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-gcc-ar \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-gcc-ar-12 \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-gcc-nm \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-gcc-nm-12 \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-gcc-ranlib \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-gcc-ranlib-12 \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-gcov \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-gcov-12 \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-gcov-dump \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-gcov-dump-12 \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-gcov-tool \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-gcov-tool-12 \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-gprof \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-ld \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-ld.bfd \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-ld.gold \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-lto-dump \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-lto-dump-12 \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-nm \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-objcopy \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-objdump \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-ranlib \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-readelf \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-size \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-strings \
&& ln -s ./chroot_exec /usr/local/bin/aarch64-linux-gnu-strip \
&& ln -s ./chroot_exec /usr/local/bin/gzip \
&& ln -s ./chroot_exec /usr/local/bin/xz

COPY ./run.sh /run.sh
CMD ["/run.sh"]
