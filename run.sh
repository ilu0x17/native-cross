#!/bin/bash

set -x
set -e

export LC_ALL=C.UTF-8
export DEBIAN_FRONTEND=noninteractive

# use eatmydata to prevent excessive sync calls from package install
export LD_PRELOAD=libeatmydata.so
export LD_LIBRARY_PATH=/usr/lib/libeatmydata

WORK="/work"

PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"
export PATH

export CHROOT=/chroot/amd64

#FIXME: this is needed work around 'detected dubious ownership' error
git config --global --add safe.directory "$WORK/$PACKAGE"

export "PATH=/usr/lib/ccache/:$PATH"

cd "$WORK/$PACKAGE"
$BUILD_CMD