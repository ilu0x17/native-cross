# native-cross

This container is intended to build a arm64 kernel on a amd64 host. It uses an
arm64 container to build the kernel and to create the package. To reduce the
build time the CROSS_COMPILE feature of the kernel is used to redirect compiler
calls to a aarch64 cross toolchain. The toolchain is located in an amd64
changeroot.

Build the `chroot_exec` helper:

```bash
aarch64-linux-gnu-gcc --static -O2 -std=gnu99 -o chroot_exec chroot_exec.c
```

Build the container:

```bash
docker build -t native-cross .
```

The container can be used with the `native-cross.sh` script. The script doesn't
do any `CROSS_COMPILE` magic. You need to setup the CROSS_COMPILE variable your
self.
